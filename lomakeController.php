<?php
require_once("./dbconn.php");
require_once("./logwriter.php");
require_once("./tietoRajapinta.php");

$palautus = [];

function tekstinTalletus($yhteys, $teksti) {
    $palautus["success"] = false; 
    if(empty($_POST)) {
        lokita("Dataa ei saatu");
    } else {
        lokita(json_encode($_POST));
        $teksti = $_POST["teksti"];
        if(talleta_teksti($teksti, $yhteys)) {
            $palautus["success"] = true;
        } 
    }
    return $palautus;
}


$operaatio; 
if(empty($_POST["operaatio"])) {
    $operaatio = false; 
} else {
    $operaatio = $_POST["operaatio"];
}

switch ($operaatio) {
    case 'talletus':
        $palautus = tekstinTalletus($yhteys, $_POST["teksti"]);
        break;
    case 'haku':
        $palautus = ["success" => true, "tekstit" => hae_tekstit($yhteys)];
        break;
    default:
        $palautus = ["success" => false, "info" => "Unknown operation"];
        break;
}


header('Access-Control-Allow-Origin: *');
header("Content-type: application/json; charset=utf-8");
echo(json_encode($palautus));
?>