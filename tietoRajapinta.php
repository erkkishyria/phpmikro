<?php
require_once("logwriter.php");

/**
 * Tallettaa käyttäjältä tulleen tekstin tietokantaan. 
 * 
 * @param string $teksti Talletettava teksti 
 * @param PDO $yhteys
 * @return bool palauttaa totuusarvon onnistuiko tekstin tallennus vai ei
 */
function talleta_teksti($teksti, $yhteys) {
    $sql = "INSERT INTO tekstit (teksti) VALUES (:teksti)";
    $stmt = $yhteys->prepare($sql); 
    $stmt->bindValue(":teksti", $teksti, PDO::PARAM_STR); 
    try {
        $stmt->execute();
    } catch (\Error $e) {
        lokita("Virhe tekstin tallennuksessa: " + $e);
        return false; 
    }
    return true;
}

/**
 * Palauttaa kaikki tietokantaan tallennetut tekstit
 * @param PDO $yhteys
 */
function hae_tekstit($yhteys) {
    $sql = "SELECT * FROM tekstit";
    $stmt = $yhteys->prepare($sql); 
    $stmt->execute(); 
    return $stmt->fetchAll(PDO::FETCH_ASSOC); 
}

?>