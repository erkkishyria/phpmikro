<?php

/**
 * Kirjoittaa kansioon "logs" kullekin päivälle lokia
 * 
 * @param string $teksti 
 */
function lokita($teksti) {
    $dateTimeStr = date("Y-m-d H:i:s");
    $fileName = date("Ymd")."_log.txt";
    $file = fopen("./logs/$fileName", "a");
    $logStr = $dateTimeStr." ".$teksti."\r\n";
    fwrite($file, $logStr);
    fclose($file);
}

?>