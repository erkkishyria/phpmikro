
const controller = "./lomakeController.php";

/**
 * Lähettää lomakkeen mikropalvelulle.
 */
const postIt = () => {
    $.post(controller, {
        operaatio: "talletus",
        teksti: $("#teksti").val()
    }).done( data => {
        console.table(data);
    });
}

const haeTekstit = () => {
    $.post(controller, {
        operaatio: "aku"
    }).done( data => {
        if(data.success) {
            console.table(data.tekstit);
            //TODO: näytä tulokset taulukkomuodossa tulosalueella.
        } else {
            $(".result").html("Haku ei onnistunut...");
            setTimeout(() => {
                $(".result").html("");
            }, 3000)
        }
    });
}